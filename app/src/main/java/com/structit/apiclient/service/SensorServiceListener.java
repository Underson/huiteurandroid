package com.structit.apiclient.service;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

public class SensorServiceListener implements SensorEventListener {
    private static final String LOG_TAG = SensorServiceListener.class.getSimpleName();

    private HardwareService mService;

    public SensorServiceListener(HardwareService service){
        this.mService = service;
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.i(LOG_TAG, "Sensor value" + event.values[0]);
        this.mService.notifyLight(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.i(LOG_TAG, "Sensor accuracy" + accuracy);
    }
}
