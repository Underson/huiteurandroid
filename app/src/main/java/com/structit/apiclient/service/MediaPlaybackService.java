//package com.structit.apiclient.service;
//
//import android.os.Bundle;
//import android.service.media.MediaBrowserService;
//import androidx.core.media.MediaBrowserCompat;
//import androidx.core.media.session.MediaSessionCompat;
//import androidx.core.media.session.PlaybackStateCompat;
//
//import java.util.List;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.media.MediaBrowserServiceCompat;
//MediaBrowserCompat
//public class MediaPlaybackService extends MediaBrowserServiceCompat {
//    private static final String MY_MEDIA_ROOT_ID = "media_root_id";
//    private static final String MY_EMPTY_MEDIA_ROOT_ID = "empty_root_id";
//
//    private MediaSessionCompat mMediaSession;
//    private PlaybackStateCompat.Builder mStateBuilder;
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        // Create a MediaSessionCompat
//        mMediaSession = new MediaSessionCompat(this, "un string");
//
//        // Enable callbacks from MediaButtons and TransportControls
//        mMediaSession.setFlags(
//                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
//                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
//
//        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
//        mStateBuilder = new PlaybackStateCompat.Builder()
//                .setActions(
//                        PlaybackStateCompat.ACTION_PLAY |
//                                PlaybackStateCompat.ACTION_PLAY_PAUSE);
//        mMediaSession.setPlaybackState(mStateBuilder.build());
//
//        // MySessionCallback() has methods that handle callbacks from a media controller
//        //mMediaSession.setCallback(new MySessionCallback());
//
//        // Set the session's token so that client activities can communicate with it.
//        setSessionToken(mMediaSession.getSessionToken());
//    }
//
//    @Nullable
//    @Override
//    public MediaBrowserService.BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
//        return null;
//    }
//
//    @Override
//    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
//
//    }
//}
