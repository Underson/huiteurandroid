package com.structit.apiclient.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.structit.apiclient.R;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class HardwareService extends Service {


    public static void stopService() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static final String ACTION_LOCATION = "LOCATION";
    public static final String ACTION_LOCATION_EXTRA_PROVIDER = "LOCATION_PROVIDER";
    public static final String ACTION_LOCATION_EXTRA_LATITUDE = "LOCATION_LATITUDE";
    public static final String ACTION_LOCATION_EXTRA_LONGITUDE = "LOCATION_LONGITUDE";

    public static final String ACTION_LIGHT = "LIGHT";
    public static final String ACTION_LIGHT_VALUE = "LIGHT_VALUE";
    private static final int NOTIFICATION_CHANNEL_ID = 1;


    @Override
    public void onCreate() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder notificationBuilder = new Notification.Builder(this, Integer.toString(NOTIFICATION_CHANNEL_ID));
            Notification notification = notificationBuilder.build();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                startMyOwnForeground();
            else{
                startForeground(NOTIFICATION_CHANNEL_ID, notification);
            }
        } //Else do nothing
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SensorManager sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        SensorServiceListener sensorListener = new SensorServiceListener(this);


        try{
            sensorManager.registerListener(
                    sensorListener,
                    sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT),
                    SensorManager.SENSOR_DELAY_NORMAL
            );

            Log.e("MaxLight", String.valueOf(sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT).getMaximumRange()));

        }catch(SecurityException ex){
            Log.e("SECC", ex.getMessage());
        }

        Log.e("locationService","STARTTT");
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager)
                this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationServiceListener locationListener = new LocationServiceListener(this);

        // Register the listener with the Location Manager to receive location updates
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    10, 1, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    10, 1, locationListener);
        } catch(SecurityException ex) {
            Log.e("HardWareService", ex.getMessage());
        }



        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground(){
        String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    public void notifyLocation(Location location) {
        Intent intent = new Intent(HardwareService.ACTION_LOCATION);
        intent.putExtra(HardwareService.ACTION_LOCATION_EXTRA_PROVIDER, location.getProvider());
        intent.putExtra(HardwareService.ACTION_LOCATION_EXTRA_LATITUDE, location.getLatitude());
        intent.putExtra(HardwareService.ACTION_LOCATION_EXTRA_LONGITUDE, location.getLongitude());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void notifyLight(SensorEvent event){
        Intent intent = new Intent(ACTION_LIGHT);
        intent.putExtra(ACTION_LIGHT_VALUE, event.values[0]);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
