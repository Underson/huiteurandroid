package com.structit.apiclient;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.structit.apiclient.data.PlayItem;
import com.structit.apiclient.service.ApiService;
import com.structit.apiclient.service.HardwareService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private MyBroadcastReceiver mBroadcastReceiver;
    private Float lastLightValue;
    private Context context;
    private Intent intentHardwareService;
    private HardwareService hardwareService;
    private MediaPlayer mp2;
    private int numberMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(LOG_TAG, "Creating...");

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        intentHardwareService = new Intent(this, HardwareService.class);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    protected void onStop() {
        super.onStop();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        Log.i(LOG_TAG, "Starting...");

        super.onStart();
        context = this;

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        } else {
            Log.i(LOG_TAG, "ACCESS_COARSE_LOCATION and ACCESS_FINE_LOCATION authorization granted");
        }


        this.mBroadcastReceiver = new MyBroadcastReceiver(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mBroadcastReceiver,new IntentFilter(HardwareService.ACTION_LIGHT));
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mBroadcastReceiver,new IntentFilter(HardwareService.ACTION_LOCATION));

        final Button buttonPlay = findViewById(R.id.buttonPlay);
        final Button buttonPause = findViewById(R.id.buttonPause);
        final Button buttonNext = findViewById(R.id.buttonNext);

        final List<PlayItem> musicList = getAllAudioFromDevice(context);
        final Random r = new Random();
        Log.e("numberMusic", String.valueOf(musicList.size()));

        if(musicList.size() > 0){

            numberMusic = r.nextInt(musicList.size());

            final TextView view = findViewById(R.id.textViewMusicListenning);
            mp2 = MediaPlayer.create(context, Uri.parse(musicList.get(numberMusic).getUrl()));

            buttonPlay.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if(!musicList.isEmpty()){
                        view.setText(musicList.get(numberMusic).getAuthor() + " - " + musicList.get(numberMusic).getName());
                        mp2.start();
                    }
                }
            });

            buttonPause.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mp2.pause();
                }
            });

            buttonNext.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    numberMusic = r.nextInt(musicList.size());
                    mp2.reset();
                    view.setText(musicList.get(numberMusic).getAuthor() + " - " + musicList.get(numberMusic).getName());
                    Log.e("ClickNext",musicList.get(numberMusic).getAuthor() + " - " + musicList.get(numberMusic).getName());

                    mp2 = MediaPlayer.create(context, Uri.parse(musicList.get(numberMusic).getUrl()));
                    mp2.start();
                }
            });
        }


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            startForegroundService(intentHardwareService);
        }else{
            startService(intentHardwareService);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        Log.i(LOG_TAG, "Attempting to log in...");

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            Intent intent = new Intent(this, ApiService.class);
            intent.putExtra("name", email);
            intent.putExtra("pwd", password);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }
    }

    private boolean isEmailValid(String email) {
        return email.length() > 0;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 0;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void displayLocation(String provider, double lat, double lng) {

        TextView providerTextView = (TextView) findViewById(R.id.location_provider_id);
        providerTextView.setText(provider);

        TextView latitudeTextView = (TextView) findViewById(R.id.location_latitude_id);
        latitudeTextView.setText("" + lat);

        TextView longitudeTextView = (TextView) findViewById(R.id.location_longitude_id);
        longitudeTextView.setText("" + lng);
    }

    public void setVolume(Float light) {
            AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if(light > 400){
                light = 400f;
            }

            final int volume = Math.round(light/20);
            Log.e("volume", String.valueOf(volume));

            if(volume != audio.getStreamVolume(audio.STREAM_MUSIC)){
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_SHOW_UI);
            }
    }

    public List<PlayItem> getAllAudioFromDevice(final Context context) {

        final List<PlayItem> tempAudioList = new ArrayList<>();

        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST,};
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor c = this.managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);

        if (c != null) {
            int count = 0;
            while (c.moveToNext()) {
                Log.d("move","MoveToNext");

                String path = c.getString(0);
                String album = c.getString(1);
                String artist = c.getString(2);
                String name = path.substring(path.lastIndexOf("/") + 1);

                PlayItem audioModel = new PlayItem(count,name,artist,album,path);
                count++;


//                if(artist.equals("Narcos")){
//                    Log.e("Name :" + name, " Album :" + album);
//                    Log.e("Path :" + path, " Artist :" + artist);
//                }
                tempAudioList.add(audioModel);
            }
            c.close();
        }

        return tempAudioList;
    }
}

