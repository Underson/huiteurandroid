package com.structit.apiclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.structit.apiclient.service.HardwareService;

public class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = MyBroadcastReceiver.class.getSimpleName();

    private LoginActivity mActivity;

    public MyBroadcastReceiver(LoginActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.e("getAction",intent.getAction());
        if(intent.getAction() == HardwareService.ACTION_LOCATION) {
            Log.i(LOG_TAG, "Intent received : " + intent.getAction());
            this.mActivity.displayLocation(
                    intent.getStringExtra(HardwareService.ACTION_LOCATION_EXTRA_PROVIDER),
                    intent.getDoubleExtra(HardwareService.ACTION_LOCATION_EXTRA_LATITUDE,
                            0.0),
                    intent.getDoubleExtra(HardwareService.ACTION_LOCATION_EXTRA_LONGITUDE,
                            0.0));
        } else if(intent.getAction() == HardwareService.ACTION_LIGHT){
            Float light = intent.getFloatExtra(HardwareService.ACTION_LIGHT_VALUE,0);

            this.mActivity.setVolume(light);
        }
        else{
            Log.w(LOG_TAG, "Unknown intent received : " + intent.getAction());
        }
    }
}
